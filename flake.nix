{
  description = "A python spreadsheet program";

  inputs = {
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
      in
      rec {
        # `nix build`
        packages.pysheet = pkgs.poetry2nix.mkPoetryApplication {
          projectDir = ./.;
        };
        defaultPackage = packages.pysheet;

        # `nix run`
        apps.pysheet = utils.lib.mkApp {
          drv = packages.pysheet;
        };
        defaultApp = apps.pysheet;

        # `nix develop`
        devShell = pkgs.mkShell {
          buildInputs = nixpkgs.lib.singleton (pkgs.poetry2nix.mkPoetryEnv {
            projectDir = ./.;
          });
          nativeBuildInputs = with pkgs; [ poetry ];
        };
      });
}

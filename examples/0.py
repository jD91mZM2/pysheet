import pysheet
from pysheet import Row, HeaderRow, HeaderBar
from dataclasses import dataclass

EXCHANGE_RATE = 8.36


def main():
    example = MonthlyPrice("example", 0)

    # | service   | usd_monthly | usd_yearly | sek_monthly | sek_yearly  |
    # |-----------|-------------|------------|-------------|-------------|
    # | Ice Cream |       5.000 |     60.000 |      41.800 |     501.600 |
    # | Candles   |   50000.000 | 600000.000 |  418000.000 | 5016000.000 |
    pysheet.render_here(
        [
            HeaderRow(example),
            HeaderBar(example),
            MonthlyPrice("Ice Cream", 5),
            MonthlyPrice("Candles", 50000),
        ]
    )


@dataclass
class MonthlyPrice(Row):
    row_header = ["service", "usd_monthly", "usd_yearly", "sek_monthly", "sek_yearly"]

    service: str
    usd_monthly: int

    def usd_yearly(self):
        return self.usd_monthly * 12

    def sek_monthly(self):
        return self.usd_monthly * EXCHANGE_RATE

    def sek_yearly(self):
        return self.usd_yearly() * EXCHANGE_RATE

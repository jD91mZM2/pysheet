from itertools import takewhile
from os import SEEK_SET
import inspect

# Reexports
from .row import Row, HeaderRow, HeaderBar
from .table import render_str

# Don't warn about the reexports
assert Row
assert HeaderRow
assert HeaderBar


def render_here(rows):
    # Get caller filename/lineno
    stack = inspect.stack()
    caller = inspect.getframeinfo(stack[1].frame)
    filename = caller.filename
    lineno = caller.lineno - 1

    # Read file
    with open(filename, "r+") as f:
        content = f.read()
        lines = content.splitlines(True)

        previous_lines = lines[:lineno]
        line = lines[lineno]
        next_lines = lines[lineno:]

        # Overwrite existing table
        while previous_lines[-1].lstrip().startswith("# "):
            previous_lines.pop()

        # Figure out indentation
        indent = "".join(takewhile(lambda s: s.isspace(), line))

        # Render table
        table = render_str(rows)
        table_lines = map(lambda l: indent + "# " + l, table.splitlines(True))

        # Figure out where to place written table back
        previous_pos = sum(map(lambda s: len(s), previous_lines))

        # Write table & remaining lines in file
        f.seek(previous_pos, SEEK_SET)
        f.writelines(table_lines)
        f.writelines(next_lines)
        f.truncate()

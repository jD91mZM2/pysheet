def render_str(table):
    if not table:
        return "[]\n"

    # Calculate amount of cells in each row
    table_cells = max(map(lambda row: len(row), table))

    # Calculate column widths
    cell_widths = [0] * table_cells

    for row in table:
        for x, cell in enumerate(row):
            min_width = row.cell_min_width(x, cell)
            cell_widths[x] = max(cell_widths[x], min_width)

    # Render table
    output = ""

    for y, row in enumerate(table):
        output += "|"
        for x, cell in enumerate(row):
            output += row.cell_format(x, cell, cell_widths[x])
            output += "|"
        output += "\n"

    return output

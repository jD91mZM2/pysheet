def get_field(cls, field):
    field = getattr(cls, field)

    if callable(field):
        field = field()

    return field


class Row:
    def __iter__(self):
        return map(lambda f: get_field(self, f), self.row_header)

    def __len__(self):
        return len(self.row_header)

    def cell_min_width(self, i, value):
        return len(self.cell_format(i, value, 0))

    def cell_format(self, i, value, width):
        width -= 2  # padding

        if type(value) == float or type(value) == int:
            content = f"{value:.3f}".rjust(width)
        else:
            content = str(value).ljust(width)

        return " " + content + " "


class HeaderRow(Row):
    def __init__(self, example):
        self.example = example

    def __iter__(self):
        return iter(self.example.row_header)

    def __len__(self):
        return len(self.example.row_header)


class HeaderBar(HeaderRow):
    def cell_format(self, i, value, width):
        return "-" * width

# pysheet

Simple python based spreadsheet that edits the python file itself to display
the results.

**Note: The comments in the code below were not placed manually. They were
automatically inserted.**

Example:

``` python
import pysheet
from pysheet import Row
from dataclasses import dataclass

EXCHANGE_RATE = 8.36


def main():
    # | usd_monthly |  usd_yearly | sek_monthly |    sek_yearly |
    # |-------------|-------------|-------------|---------------|
    # |       5.000 |      60.000 |      41.800 |       501.600 |
    # |  50_000.000 | 600_000.000 | 418_000.000 | 5_016_000.000 |
    pysheet.render_here(
        [
            MonthlyPrice("Ice Cream", 5),
            MonthlyPrice("Candles", 50000),
        ]
    )


@dataclass
class MonthlyPrice(Row):
    row_header = ["service", "usd_monthly", "usd_yearly", "sek_monthly", "sek_yearly"]
    row_formatter = "{:_.3f}"

    service: str
    usd_monthly: int

    def usd_yearly(self):
        return self.usd_monthly * 12

    def sek_monthly(self):
        return self.usd_monthly * EXCHANGE_RATE

    def sek_yearly(self):
        return self.usd_yearly() * EXCHANGE_RATE
```
